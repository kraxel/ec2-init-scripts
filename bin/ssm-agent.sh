#!/bin/sh

if test -x "/usr/bin/dnf"; then
    dnf="/usr/bin/dnf"
else
    dnf="/usr/bin/yum"
fi

base="https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest"
case "$(uname -m)" in
    aarch64)
	echo "# install arm ssm-agent"
	$dnf install -y "$base/linux_arm64/amazon-ssm-agent.rpm"
	;;
    x86_64)
	echo "# install x86 ssm-agent"
	$dnf install -y "$base/linux_amd64/amazon-ssm-agent.rpm"
	;;
    *)
	echo "# unknown arch"
	exit 1
	;;
esac

echo "# enable ssm-agent"
systemctl daemon-reload
systemctl enable --now  amazon-ssm-agent
