#!/bin/sh

echo "# update packages"
dnf update -y

echo "# enable auto updates"
dnf install -y dnf-automatic
systemctl enable dnf-automatic-download.timer
systemctl enable dnf-automatic-install.timer

