#!/bin/sh
cd $(dirname $0)
source ./functions

# note: fedora only
echo "# install docker"
rpminst moby-engine

no_selinux

echo "# disable cgroups v2"
grubby --update-kernel=ALL --args systemd.unified_cgroup_hierarchy=0

echo "# *** needs reboot to apply changes ***"
