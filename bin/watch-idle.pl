#!/usr/bin/perl
use strict;
use warnings;

my $limit = shift;
my $verbose = shift;

my $idle = 0;
$limit = 15 unless defined($limit);
$verbose = 1 unless defined($verbose);

STDOUT->autoflush(1);
while (1) {
	open FILE, "<", "/proc/loadavg" or die "open /proc/loadavg: $!";
	my $loadavg = <FILE>;
	close FILE;
	open FILE, "<", "/proc/uptime" or die "open /proc/uptime: $!";
	my $uptime = <FILE>;
	close FILE;

	my ($m1, $m5, $m15) = split /\s+/, $loadavg;
	my ($upsecs) = split /\./, $uptime;
	my $upmins = int($upsecs / 60);
	my $ts = sprintf("up %d:%02d", $upmins / 60, $upmins % 60);

	if ($m1 eq "0.00" and 
	    $m5 eq "0.00" and
	    $m15 eq "0.00") {
		$idle++;
		printf("[%s] idle: %d/%d min\n",
		       $ts, $idle, $limit)
		    if $verbose >= 1;
	} elsif ($idle) {
		$idle = 0;
		printf("[%s] idle: reset (load %s)\n", $ts, $m1)
		    if $verbose >= 1;
	} else {
		printf("[%s] load: %s - %s - %s\n",
		       $ts, $m1, $m5, $m15)
		    if $verbose >= 2;
	}

	if ($idle >= $limit) {
		printf("[%s] limit (%d min) reached, poweroff\n", $ts);
		system("/sbin/poweroff");
	}

	sleep 60;
}
