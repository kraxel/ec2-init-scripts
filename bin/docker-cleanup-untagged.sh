#!/bin/sh
untagged=$(docker images --filter dangling=true --quiet)
for image in $untagged; do
    echo "# untagged: $image"
    docker rmi $image
done
