#!/bin/sh

echo "# check epel install"
if rpm -q epel-release; then
    echo "# epel is enabled"
    exit
fi

source /etc/os-release
echo "# enable epel for ${ID}-${VERSION_ID}"

EPEL_URL="https://dl.fedoraproject.org/pub/epel/epel-release-latest-${VERSION_ID}.noarch.rpm"
if test -x /usr/bin/dnf; then
    echo "# dnf install ${EPEL_URL}"
    dnf install -y ${EPEL_URL}
else
    echo "# yum install ${EPEL_URL}"
    yum install -y ${EPEL_URL}
fi
