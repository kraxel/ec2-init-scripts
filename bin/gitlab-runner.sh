#!/bin/sh
cd $(dirname $0)
source ./functions
base="$(git rev-parse --show-toplevel)"
sdir="${base%/*}/gitlab-runner"

if test -d "${sdir}/.git"; then
    echo "# update gitlab-runner in $sdir"
    cd "$sdir"
    git pull
else
    echo "# clone gitlab-runner to $sdir"
    git clone https://gitlab.com/gitlab-org/gitlab-runner.git "$sdir"
    cd "$sdir"
fi

release=$(git tag --sort=taggerdate | grep ^v | grep -v rc | tail -1)
echo "# checkout latest release $release"
git checkout $release

rpminst golang

echo "# build gitlab-runner"
go build -v || exit 1

echo "# install gitlab-runner"
rm -v -f /usr/local/bin/gitlab-runner
cp -v gitlab-runner /usr/local/bin

echo "# configure gitlab-runner"
cd $base
mkdir -p /etc/gitlab-runner
touch /etc/gitlab-runner/config.toml
cp -v systemd/gitlab-runner.service /etc/systemd/system
systemctl daemon-reload
systemctl enable --now gitlab-runner
