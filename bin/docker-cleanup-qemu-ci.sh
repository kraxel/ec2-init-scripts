#!/bin/sh
qemu=$(docker images --quiet "registry.gitlab.com/kraxel/qemu/qemu/*")
for image in $qemu; do
    echo "# qemu: $image"
    docker rmi $image
done
