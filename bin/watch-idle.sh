#!/bin/sh
cd $(dirname $0)
base="$(git rev-parse --show-toplevel)"
source ./functions

no_selinux

rpminst perl-interpreter
rpminst perl-IO
cp -v $base/systemd/watch-idle.service /etc/systemd/system
systemctl daemon-reload

echo "# enable watch-idle"
systemctl enable --now watch-idle
