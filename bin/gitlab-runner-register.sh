#!/bin/sh

if test ! -f /etc/sysconfig/gitlab-runner; then
    echo "not found: /etc/sysconfig/gitlab-runner"
    exit 1
fi
source /etc/sysconfig/gitlab-runner

########################################################################

args=()
args+=("--non-interactive")
args+=("--url" "${GITLAB_SERVER-https://gitlab.com}")

# arch tags
tags="$(uname -m)"
case "$(uname -m)" in
    x86_64)
	tags="$tags,amd64"
	args+=("--run-untagged")
	;;
    aarch64)
	tags="$tags,arm64"
	;;
    armv7*)
	tags="$tags,arm"
	;;
esac

# distro tags
if test -f /etc/os-release; then
    source /etc/os-release
    tags="$tags,${ID}"
    tags="$tags,${ID}-${VERSION_ID}"
fi

# executor setup + tags
echo "# check for docker"
if systemctl is-active docker.socket; then
    mode="docker"
    tags="$tags,docker"
    args+=("--executor" "docker")
    args+=("--docker-image" "docker.io/alpine:latest")
    if true; then
	tags="$tags,docker-priv"
	args+=("--docker-privileged")
    fi
else
    mode="shell"
    tags="$tags,shell"
    args+=("--executor" "shell")
fi

# feature tags
if rpm -q qemu-user-static; then
    tags="$tags,qemu-user"
fi

# cloud tags
if test -x /usr/bin/cloud-init; then
    platform=$(cloud-init query platform)
    instance=$(cloud-init query instance_id)
    tags="$tags,$platform"
    args+=("--name" "$instance")
fi

args+=("--tag-list" "$tags")

echo "# register gitlab-runners ($mode)"
count=0
total=$(echo ${GITLAB_TOKENS} | wc -w)
for token in ${GITLAB_TOKENS}; do
    count=$(( $count + 1 ))
    echo "#   $count/$total ..."
    /usr/local/bin/gitlab-runner register "${args[@]}" --registration-token "$token"
done
